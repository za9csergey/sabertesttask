﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaberTestTask
{
    class Program
    {
        static void Main(string[] args)
        {
            // fill list with 10 nodes
            ListRand list = new ListRand();
            for (int i = 1; i <= 10; i++)
            {
                list.AddNewItem("node" + i);
            }

            // serialize list
            using (FileStream fs = new FileStream(@"output.txt", FileMode.Open, FileAccess.Write))
            {
                list.Serialize(fs);
            }
            list.PrintList();

            Console.WriteLine(list.Count);

            // deserialize 
            using (FileStream fs = new FileStream(@"input.txt", FileMode.Open, FileAccess.Read))
            {
                list.Deserialize(fs);
            }
            list.PrintList();
            Console.ReadKey();
        }
    }
}
