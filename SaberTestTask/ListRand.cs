﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SaberTestTask
{
    public class ListRand
    {
        public ListNode Head;
        public ListNode Tail;
        public int Count;
        Random random = new Random();

        /// <summary>
        /// Serialization
        /// </summary>
        /// <param name="s"></param>
        public void Serialize(FileStream s)
        {
            StringBuilder sb = new StringBuilder(30);
            for (ListNode node = Head; node != null; node = node.Next)
            {
                Byte[] nodeBytes = node.DataToSave;
                s.Write(nodeBytes, 0, nodeBytes.Length);
            }
        }

        /// <summary>
        /// Deserialization from file
        /// </summary>
        /// <param name="s"></param>
        public void Deserialize(FileStream s)
        {
            // free list
            Head = null;
            Tail = null;
            List<string> randomStrings = new List<string>();

            using (TextReader tr = new StreamReader(s))
            {
                // read lines from file
                string line;
                while ((line = tr.ReadLine()) != null)
                {
                    // split line by ',' on Data and random
                    string[] data = line.Split(',');
                    // add item to list without random member
                    AddToTail(data[0]);
                    randomStrings.Add(data[1]);
                    Count++;
                }
            }

            // initialize random member of node from file
            int index = 0;
            for (ListNode node = Head; node != null; node = node.Next, index++)
            {
                for (ListNode anotherNode = Head; anotherNode != null; anotherNode = anotherNode.Next)
                {
                    if (anotherNode.Data.Equals(randomStrings[index]))
                    {
                        node.Rand = anotherNode;
                    }
                }
            }
        }

        /// <summary>
        /// Adds new item to linked list
        /// </summary>
        /// <param name="data">item to add</param>
        public void AddNewItem(string data)
        {
            ListNode newNode = AddToTail(data);
            InitizlizeRandomNode(newNode);
            
        }

        /// <summary>
        /// Adds item to taim of the list
        /// </summary>
        /// <param name="data">item to add</param>
        /// <returns>new node of linked list</returns>
        private ListNode AddToTail(string data)
        {
            // create new node
            ListNode newNode = new ListNode(data);
            newNode.Prev = Tail;
            newNode.Next = null;
            Count++;

            // if list is empty new item is a head
            if (Tail == null)
            {
                Head = newNode;
            }
            else
            {
                // else change tail's next pointer to new node
                Tail.Next = newNode;
            }

            // assing Tail to new node
            Tail = newNode;
            return newNode;
        }

        /// <summary>
        /// assings random node from list to new node
        /// </summary>
        /// <param name="newNode"></param>
        private void InitizlizeRandomNode(ListNode newNode)
        {
            // get random number
            int randomNumber = random.Next(1, Count);

            // find item with randomNumber in list
            ListNode tmp = Head;
            for (int i = 1; i != randomNumber; i++, tmp = tmp.Next) { }

            newNode.Rand = tmp;
        }

        /// <summary>
        /// Prints list's content in console
        /// </summary>
        public void PrintList()
        {
            for (ListNode node = Head; node != null; node = node.Next)
            {
                Console.WriteLine(node.Data + ", " + node.Rand.Data);
            }
        }
    }
}
