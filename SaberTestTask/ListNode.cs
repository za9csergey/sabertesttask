﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaberTestTask
{
    public class ListNode
    {
        public ListNode Prev;
        public ListNode Next;
        public ListNode Rand; // произвольный элемент внутри списка
        public string Data;
        
        // converts Data and Rand.Data string to byte array
        public byte[] DataToSave
        {
            get
            {
                string result = Data + "," + Rand.Data;
                return Encoding.ASCII.GetBytes(result.ToString()).
                    Concat(Encoding.ASCII.GetBytes(Environment.NewLine)).ToArray();
            }
        }

        public ListNode(string data)
        {
            Data = data;
        }


    }
}
